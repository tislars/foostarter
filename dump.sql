# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.30-1+deb.sury.org~trusty+2)
# Database: gorinchembeweegt
# Generation Time: 2016-06-07 10:15:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table content_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content_item`;

CREATE TABLE `content_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `language_group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `content_type` (`type`),
  KEY `IDX_D279C8DB82F1BAF4` (`language_id`),
  KEY `IDX_D279C8DB92F6817D` (`language_group_id`),
  CONSTRAINT `FK_D279C8DB82F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_D279C8DB92F6817D` FOREIGN KEY (`language_group_id`) REFERENCES `content_item_language_group` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `content_item` WRITE;
/*!40000 ALTER TABLE `content_item` DISABLE KEYS */;

INSERT INTO `content_item` (`id`, `type`, `content`, `created_on`, `language_id`, `language_group_id`)
VALUES
	(89,'home','{\"title\":\"Home\",\"subtitle\":\"Dit is de homepage\",\"intro\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;<span style=\\\"line-height: 1.42857;\\\">tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp;<\\/span><span style=\\\"line-height: 1.42857;\\\">quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo&nbsp;<\\/span><span style=\\\"line-height: 1.42857;\\\">consequat.<\\/span><\\/p>\",\"body\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. D<span style=\\\"line-height: 1.42857;\\\">olor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.<\\/span><\\/p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<span style=\\\"line-height: 1.42857;\\\"><br><\\/span><\\/p>\",\"footer_text\":\"asdads asd as\"}','2015-11-19 14:24:28',1,NULL),
	(148,'infopage','{\"title\":\"Infopagina\",\"subtitle\":\"Dit is een infopagina\",\"intro\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam<\\/p>\",\"body\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Dolor sit amet, consectetur adipisicing elit.&nbsp;<\\/p><p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.<\\/p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\\/p>\"}','2016-01-21 13:59:37',1,NULL),
	(149,'infopage','{\"title\":\"Contact\",\"subtitle\":\"Te gekke contact pagina\",\"intro\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<br><\\/p>\",\"body\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<\\/p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<br><\\/p>\"}','2016-01-21 14:04:39',1,NULL),
	(150,'infopage','{\"title\":\"Over ons\",\"subtitle\":\"Subtitel lorem ipsum dolor sit amet\",\"intro\":\"<p>test<\\/p>\",\"body\":\"<p>test<\\/p>\"}','2016-02-09 10:07:13',1,NULL),
	(151,'infopage','{\"title\":\"Things about quick brown foxes that jump over lorem ipsums\",\"subtitle\":\"Someone lorem that ipsum before it runs over the quick brown fox!\",\"intro\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris\",\"body\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br><\\/p>\"}','2016-02-09 10:18:35',1,NULL),
	(152,'infopage','{\"title\":\"Excepteur sint occaecat cupidatat non proident\",\"intro\":\"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\",\"body\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tristique felis tellus, ac fermentum turpis vulputate cursus. Quisque eget eros ac mi lobortis mollis in ut mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dignissim vulputate tortor ac lobortis. Cras ac ante ut sapien mollis porta eu vel leo. Aenean varius leo quis nulla vehicula, quis euismod diam interdum. Aenean et justo nec tortor volutpat volutpat. Curabitur et risus at mi rutrum posuere. Duis neque purus, condimentum quis dolor sed, tincidunt fringilla lorem. Proin gravida urna nec orci porttitor, sed condimentum arcu iaculis. Sed placerat eget sapien nec hendrerit.<\\/p><p>Nulla accumsan interdum nunc, at accumsan velit tristique ac. Vivamus vulputate convallis mi in tristique. Aliquam vehicula, purus at luctus dignissim, velit sem consectetur magna, a sagittis est nibh sed metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Fusce accumsan tincidunt ullamcorper. Nulla facilisi.<\\/p><p>Maecenas non bibendum nunc. Vivamus in adipiscing lectus, sed sagittis mi. Sed ut odio ultricies, mattis tellus nec, consectetur eros. Morbi eu interdum risus. Duis pellentesque tortor erat, eu vulputate nulla consectetur non. Aenean fermentum imperdiet enim, pulvinar volutpat magna eleifend id. Etiam condimentum, ipsum vel mollis dictum, nibh diam dictum sapien, nec aliquam purus odio vitae nulla. Vestibulum rutrum ullamcorper velit id suscipit. In pretium vel sem in feugiat. Ut id rhoncus eros.<\\/p><p>In sit amet sagittis lacus. Integer quis mi lobortis, euismod purus ac, pharetra ligula. Fusce ut orci sodales, viverra diam in, mollis justo. Aliquam feugiat facilisis accumsan. Cras viverra a ante eget vestibulum. In hac habitasse platea dictumst. Nullam mattis eleifend diam et volutpat. Nulla sed condimentum augue. Nulla mollis condimentum dolor, ut scelerisque tellus blandit pellentesque.<\\/p><p>Ut egestas venenatis turpis quis ultrices. Integer blandit nisl ut nibh semper cursus. Nulla in ligula euismod, venenatis metus a, feugiat mi. Praesent interdum, risus condimentum fermentum tincidunt, neque nisl pulvinar nulla, ut dignissim purus neque quis lacus. Aenean eget sapien eu tortor convallis pretium. Etiam felis ligula, commodo vel lacus sit amet, auctor porttitor quam. Sed mollis enim in elit luctus sagittis. Pellentesque vestibulum adipiscing lacus, at venenatis ipsum dapibus vel. Etiam rhoncus massa mollis aliquam tincidunt. Quisque quam sapien, aliquet nec nibh eu, pretium tempor sapien.<\\/p>\"}','2016-02-09 10:20:43',1,NULL),
	(153,'sidebarBlock','{\"title\":\"Link naar een leuke pagina\",\"body\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"link\":\"https:\\/\\/nu.nl\",\"button_text\":\"Ga daar heen!\"}','2016-02-09 11:00:31',1,NULL),
	(154,'sidebarBlock','{\"title\":\"Meneertje Koekepeertje\",\"body\":\"<b>Telefoon:<\\/b> ja, een nummer<p><b>Email:<\\/b> ook wel ja<\\/p>\"}','2016-02-09 11:01:05',1,NULL);

/*!40000 ALTER TABLE `content_item` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table content_item_document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content_item_document`;

CREATE TABLE `content_item_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document` int(11) DEFAULT NULL,
  `contentitem` int(11) DEFAULT NULL,
  `property` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_document_contentitem_property` (`document`,`contentitem`,`property`),
  KEY `IDX_EFBE64ACD8698A76` (`document`),
  KEY `IDX_EFBE64ACE79B6A11` (`contentitem`),
  KEY `position` (`position`),
  CONSTRAINT `FK_EFBE64ACD8698A76` FOREIGN KEY (`document`) REFERENCES `document` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_EFBE64ACE79B6A11` FOREIGN KEY (`contentitem`) REFERENCES `content_item` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `content_item_document` WRITE;
/*!40000 ALTER TABLE `content_item_document` DISABLE KEYS */;

INSERT INTO `content_item_document` (`id`, `document`, `contentitem`, `property`, `position`, `createdAt`)
VALUES
	(1,1,150,'mainImage',1,'2016-02-09 10:07:13'),
	(2,2,151,'mainImage',1,'2016-02-09 10:18:35'),
	(3,3,154,'mainImage',1,'2016-02-09 11:01:05'),
	(5,7,89,'mainImage',1,'2016-02-09 11:15:44'),
	(6,8,89,'mainImage',1,'2016-02-09 11:18:05'),
	(7,15,89,'downloads',1,'2016-02-09 19:17:08');

/*!40000 ALTER TABLE `content_item_document` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table content_item_language_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content_item_language_group`;

CREATE TABLE `content_item_language_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `document`;

CREATE TABLE `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `originalName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unqiquename` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;

INSERT INTO `document` (`id`, `name`, `size`, `path`, `createdAt`, `originalName`)
VALUES
	(1,'50e30dddef355e618d96f106551ac4ba03f3eeda.jpg',188128,'/uploads/50e30dddef355e618d96f106551ac4ba03f3eeda.jpg','2016-02-09 10:05:10','employee-1-lg.jpg'),
	(2,'a7bbd8ab24fde81faa0f8931d3b215d84111763c.jpg',110995,'/uploads/a7bbd8ab24fde81faa0f8931d3b215d84111763c.jpg','2016-02-09 10:08:17','default-header.jpg'),
	(3,'6cf20f95973b50064f2fb6ef37878845eafcb82b.jpg',6075,'/uploads/6cf20f95973b50064f2fb6ef37878845eafcb82b.jpg','2016-02-09 11:00:50','man-128.jpg'),
	(5,'46fd376e5d0138debb9c13897175661732ffd33b.jpg',110995,'/uploads/46fd376e5d0138debb9c13897175661732ffd33b.jpg','2016-02-09 11:11:34','default-header.jpg'),
	(7,'12e96f252bfce31155c9dbb28929960b8211b0fe.jpg',505765,'/uploads/12e96f252bfce31155c9dbb28929960b8211b0fe.jpg','2016-02-09 11:15:41','project.jpg'),
	(8,'f0aa8a0208ed6b42e1111441e9047a3453904e10.jpg',188128,'/uploads/f0aa8a0208ed6b42e1111441e9047a3453904e10.jpg','2016-02-09 11:18:03','employee-1-lg.jpg'),
	(9,'8150700f4b432c654ef068fae3220edec9dc7401.pdf',356864,'/uploads/8150700f4b432c654ef068fae3220edec9dc7401.pdf','2016-02-09 17:02:52','PhpStorm_ReferenceCard.pdf'),
	(10,'d91a7fa31bcf4c9e2ed2b724c03830bce777ec14.pdf',356864,'/uploads/d91a7fa31bcf4c9e2ed2b724c03830bce777ec14.pdf','2016-02-09 17:04:52','PhpStorm_ReferenceCard.pdf'),
	(11,'c004c1c2d3b85267bfc4b5bcdc8aa09775be84a9.jpg',188128,'/uploads/c004c1c2d3b85267bfc4b5bcdc8aa09775be84a9.jpg','2016-02-09 17:06:06','employee-1-lg.jpg'),
	(12,'0752badf46ade89532b8416f2f6887514570841d.jpg',313059,'/uploads/0752badf46ade89532b8416f2f6887514570841d.jpg','2016-02-09 17:08:07','pr3.jpg'),
	(13,'0ef18c26c937e35bfd738a817eb43f6a694a39ef.jpg',313059,'/uploads/0ef18c26c937e35bfd738a817eb43f6a694a39ef.jpg','2016-02-09 19:13:35','pr3.jpg'),
	(14,'b22981f8704f8a98a3874567e3f0e9e24c4f0cdc.pdf',49206,'/uploads/b22981f8704f8a98a3874567e3f0e9e24c4f0cdc.pdf','2016-02-09 19:16:37','Charge_HR_maat.pdf'),
	(15,'71e239b5a41dc80b520eea33b96601a36036320f.pdf',49206,'/uploads/71e239b5a41dc80b520eea33b96601a36036320f.pdf','2016-02-09 19:17:06','Charge_HR_maat.pdf');

/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group`;

CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6DC044C55E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `iso` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `fallback` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `prefix` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D4DB71B55E237E06` (`name`),
  UNIQUE KEY `UNIQ_D4DB71B561587F41` (`iso`),
  UNIQUE KEY `UNIQ_D4DB71B593B1868E` (`prefix`),
  KEY `is_active` (`is_active`),
  KEY `iso` (`iso`),
  KEY `position` (`position`),
  KEY `prefix` (`prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;

INSERT INTO `language` (`id`, `name`, `iso`, `fallback`, `is_active`, `is_default`, `position`, `prefix`)
VALUES
	(1,'Nederlands','nl',0,1,1,0,NULL),
	(2,'Engels','en',1,0,0,1,'en'),
	(3,'Duits','de',1,0,0,2,'de'),
	(4,'Frans','fr',1,0,0,3,'fr'),
	(5,'Italiaans','it',1,0,0,4,'it'),
	(6,'Spaans','es',1,0,0,5,'es');

/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL,
  `createdate` datetime NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_menu_key` (`key`,`language_id`),
  KEY `IDX_7D053A9382F1BAF4` (`language_id`),
  CONSTRAINT `FK_7D053A9382F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table menu_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_item`;

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `absolute_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `load_children` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `position` int(11) NOT NULL,
  `max_depth` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `node_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D754D550CCD7E912` (`menu_id`),
  KEY `IDX_D754D550460D9FD7` (`node_id`),
  CONSTRAINT `FK_D754D550460D9FD7` FOREIGN KEY (`node_id`) REFERENCES `node` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D754D550CCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table migration_versions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration_versions`;

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;

INSERT INTO `migration_versions` (`version`)
VALUES
	('20150904135812'),
	('20151102111736'),
	('20151102133657'),
	('20151102134911'),
	('20151110144712'),
	('20151110144831'),
	('20151113133201'),
	('20151113153024'),
	('20151218111447'),
	('20160112142148'),
	('20160129111313'),
	('20160129112739'),
	('20160129115249'),
	('20160204164758'),
	('20160210094733'),
	('20160212153143'),
	('20160216134031'),
	('20160216135416'),
	('20160223110957'),
	('20160223135647');

/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table node
# ------------------------------------------------------------

DROP TABLE IF EXISTS `node`;

CREATE TABLE `node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `content` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `position` int(11) NOT NULL,
  `description` varchar(170) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allow_indexing` tinyint(1) NOT NULL,
  `node_key` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_857FE845FEC530A9` (`content`),
  UNIQUE KEY `slug_path` (`slug`,`parent_id`),
  KEY `IDX_857FE845727ACA70` (`parent_id`),
  KEY `is_active` (`isActive`),
  KEY `path` (`path`),
  KEY `position` (`position`),
  KEY `node_key` (`node_key`),
  CONSTRAINT `FK_857FE845727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `node` (`id`),
  CONSTRAINT `FK_857FE845FEC530A9` FOREIGN KEY (`content`) REFERENCES `content_item` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `node` WRITE;
/*!40000 ALTER TABLE `node` DISABLE KEYS */;

INSERT INTO `node` (`id`, `parent_id`, `content`, `slug`, `isActive`, `path`, `created_on`, `position`, `description`, `keywords`, `title`, `allow_indexing`, `node_key`)
VALUES
	(1,NULL,89,'',1,'/','2015-11-19 14:24:28',0,'','','',0,NULL),
	(57,NULL,148,'infopage',1,'/infopage','2016-01-21 13:59:37',1,'','','',1,NULL),
	(58,NULL,149,'contact',1,'/contact','2016-01-21 14:04:39',2,'','','',1,NULL),
	(59,NULL,150,'test12',1,'/test12','2016-02-09 10:07:13',3,'','','',1,NULL),
	(60,57,151,'things',1,'/infopage/things','2016-02-09 10:18:35',0,'','','',1,NULL),
	(61,57,152,'test123',1,'/infopage/test123','2016-02-09 10:20:43',2,'','','',1,NULL);

/*!40000 ALTER TABLE `node` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table related_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `related_content`;

CREATE TABLE `related_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `property` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_related_content_property` (`parent_id`,`child_id`,`property`),
  KEY `IDX_C1DCFA0C727ACA70` (`parent_id`),
  KEY `IDX_C1DCFA0CDD62C21B` (`child_id`),
  KEY `position` (`position`),
  CONSTRAINT `FK_C1DCFA0C727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `content_item` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_C1DCFA0CDD62C21B` FOREIGN KEY (`child_id`) REFERENCES `content_item` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `related_content` WRITE;
/*!40000 ALTER TABLE `related_content` DISABLE KEYS */;

INSERT INTO `related_content` (`id`, `parent_id`, `child_id`, `property`, `position`, `created_on`)
VALUES
	(1,89,153,'sidebarBlocks',1,'2016-02-09 19:13:24'),
	(2,89,154,'sidebarBlocks',2,'2016-02-09 19:13:24');

/*!40000 ALTER TABLE `related_content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`)
VALUES
	(1,'admin','admin','matthijs@reprovinci.nl','matthijs@reprovinci.nl',1,'o42bd4e3f0g4sgk0oggg0sgo00o48cs','29tn7eAbITagzzgqvgci4OxS5G7X3Wd4NFSe1GaOgMc/W2wk/ZNFHZSlnpWCSnqKz/uaVZbdISwyoYxfwywGKQ==','2016-02-10 08:32:30',0,0,NULL,NULL,NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}',0,NULL);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_user_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_user_group`;

CREATE TABLE `user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_28657971A76ED395` (`user_id`),
  KEY `IDX_28657971FE54D947` (`group_id`),
  CONSTRAINT `FK_28657971A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_28657971FE54D947` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
