// /*global console,window,document,removeClass,addClass */

// // add a class to the page header when scrolling down
// // show full size again when scrolling up
// (function () {

//     'use strict';

//     var state = false,
//         lastScrollTop = 0,
//         st,
//         direction,
//         triggerOffset = 7,
//         menutrigger = 300;

//     function detectDirection() {

//         st = window.pageYOffset;

//         if (st < (lastScrollTop - triggerOffset)) {
//             direction = "up";
//         } else if (st < lastScrollTop) {
//             //console.log('scrolling up, but within offset');
//         } else {
//             direction = "down";
//         }

//         lastScrollTop = st;

//         return direction;

//     }

//     if (window.innerHeight > 600 && window.innerWidth > 767) {
//         document.addEventListener('scroll', function () {
            
            
//             var dir = detectDirection(),
//                 scrolltop = document.body.scrollTop;
            
//             //console.log(scrolltop);
            
//             if (scrolltop > 225 ) {
//                 addClass('fixed', '.site-header');
//             } else {
//                 removeClass('fixed', '.site-header');
//             }
            
//             if (dir === "down" && scrolltop > menutrigger) {
//                 addClass('out', '.site-header');
//                 removeClass('in', '.site-header');
//             } else if (dir === "up" && scrolltop > menutrigger) {
//                 removeClass('out', '.site-header');
//                 addClass('in', '.site-header');
//                 //state = 'in';
//             } else if (dir === "up" && scrolltop <= menutrigger && scrolltop > 225) {
//                 addClass('out', '.site-header');
//                 removeClass('in', '.site-header');
//                 //state = 'in';
//             } else if (scrolltop < menutrigger) {
//                 removeClass('out', '.site-header');
//                 removeClass('in', '.site-header');
//             }

//         });
//     }
// })();
