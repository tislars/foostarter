/**
 *
 * source: https://gist.github.com/mcbrwr/7fc6f4bba5f2c85c0dfa
 *
 */


// smooth scroll
$(function() {
  
  //function
  function animate(elem,style,unit,from,to,time,prop) {
    if( !elem) return;
    var start = new Date().getTime(),
      timer = setInterval(function() {
        var step = Math.min(1,(new Date().getTime()-start)/time);
        if (prop) {
          elem[style] = (from+step*(to-from))+unit;
        } else {
          elem.style[style] = (from+step*(to-from))+unit;
        }
        if( step == 1) clearInterval(timer);
      },25);
    elem.style[style] = from+unit;
  }

  // trigger
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        var toppie = target.offset().top - 150;
        $('html,body').animate({
          scrollTop: toppie
        }, 1000);
        return false;
      }
    }
  });

});