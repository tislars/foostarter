/*===============================================
=            Global functions & vars            =
===============================================*/
/*global document */

// /**
//  *
//  * Cookie functions
//  *
//  */
// function createCookie(name, value, days) {
//     'use strict';
//     var expires, date;
//     if (days) {
//         date = new Date();
//         date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
//         expires = "; expires=" + date.toGMTString();
//     } else {
//         expires = "";
//     }
//     document.cookie = name + "=" + value + expires + "; path=/";
// }

// function readCookie(name) {
//     'use strict';
//     var nameEQ = name + "=";
//     var ca = document.cookie.split(';');
//     for (var i=0;i < ca.length;i++) {
//         var c = ca[i];
//         while (c.charAt(0)==' ') c = c.substring(1,c.length);
//         if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
//     }
//     return null;
// }

// function eraseCookie(name) {
//     createCookie(name,"",-1);
// }


// /**
//  *
//  * for each element
//  *
//  */
// function forEachElement(el, fn) {
//     var i;
//     if (typeof el === 'string' || el instanceof String) {
//         // it's a string, so treat it as selector
//         var elements = document.querySelectorAll(el);
//         for (i = 0; i < elements.length; i++)
//             fn(elements[i], i);
//     } else if (typeof el === 'object') {
//         // it's an array of element objects?
//         for (i = 0; i < el.length; i++)
//             fn(el[i], i);
//     }
// }


// /**
//  *
//  * toggle class
//  * selector var is a query selector lik 'body #my .awesomeness'
//  *
//  */
// function toggleClass(className,el) {
//     forEachElement(el, function(el) {
//         if (el.classList) {
//           el.classList.toggle(className);
//         } else {
//             var classes = el.className.split(' ');
//             var existingIndex = -1;
//             for (var i = classes.length; i--;) {
//               if (classes[i] === className)
//                 existingIndex = i;
//             }

//             if (existingIndex >= 0)
//               classes.splice(existingIndex, 1);
//             else
//               classes.push(className);

//           el.className = classes.join(' ');
//         }
//     });
// }

// /**
//  *
//  * add class
//  * todo: check if class exists
//  *
//  */
//  function addClass(className,el) {

//     if (typeof el === 'string' || el instanceof String) {
//         // if el is a string
//         forEachElement(el, function(elm){
//             if (elm.classList)
//               elm.classList.add(className);
//             else
//               elm.className += ' ' + className;
//         });
//     } else {
//         // else if className is a dom object:
//         if (el !== null && typeof el === 'object')
//           el.className += ' ' + className;
//     }

//  }


// /**
//  *
//  * remove class
//  * selector var is a query selector lik 'body #my .awesomeness'
//  *
//  */
// function removeClass(className, el) {
//     if (typeof el === 'string' || el instanceof String) {
//         // if el is a string
//         forEachElement(el, function(el) {
//             if (el.classList)
//               el.classList.remove(className);
//             else
//               el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
//         });
//     } else {
//         // it's an object
//         if (el.classList)
//           el.classList.remove(className);
//         else
//           el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
//     }
// }


// /**
//  *
//  * get children
//  *
//  */
// function getChildren(n, skipMe){
//     var r = [];
//     for ( ; n; n = n.nextSibling ) 
//        if ( n.nodeType == 1 && n != skipMe)
//           r.push( n );        
//     return r;
// }

// /**
//  *
//  * get siblings
//  *
//  */
// function getSiblings(n) {
//     if (n !== null && typeof n === 'object') {
//         return getChildren(n.parentNode.firstChild, n);
//         //return false;
//     } else {
//         return false;
//     }
// }


// /**
//  *
//  * activate an element
//  * deactivate all siblings
//  *
//  */
// function activate(el) {
//     if (typeof el === 'string' || el instanceof String) {
//         // it's a string, so treat it as selector

//     } else {
//         // it's an object
//         // remove all active classes
//         var siblings = getSiblings(el);
//         forEachElement(siblings, function(elm) {
//             removeClass('active', elm);
//         });
//         //removeClass('active','.site-options_audience a');
//         // add the active class to the current element
//         addClass('active',el);
//     }
// }