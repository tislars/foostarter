console.log "-------------------------------"
console.log "    REPROVINCI GRUNT SUITE"
console.log "-------------------------------"
console.log ""
console.log "       .:::.   .:::."
console.log "      :::::::.:::::::"
console.log "      :::::::::::::::"
console.log "      ':::::::::::::'"
console.log "        ':::::::::'"
console.log "          ':::::'"
console.log "            ':'"
console.log ""


module.exports = (grunt) ->

    # make sure bower config is tip top
    grunt.file.write ".bowerrc", '{"directory": "vendors"}'

    # load modules
    grunt.loadNpmTasks "grunt-contrib-less"
    grunt.loadNpmTasks "grunt-contrib-watch"
    grunt.loadNpmTasks "grunt-contrib-uglify"
    grunt.loadNpmTasks "grunt-svg2png"
    grunt.loadNpmTasks "grunt-contrib-imagemin"
    grunt.loadNpmTasks "grunt-grunticon"
    grunt.loadNpmTasks "grunt-styleguidejs"
    grunt.loadNpmTasks "grunt-contrib-clean"
    
    # Set tasks
    grunt.registerTask "default", ["sprite","less", "watch"]
    grunt.registerTask "sprite", ["clean:spritePre","imagemin:sprite","grunticon","less","clean:spritePost"]
    grunt.registerTask "js", ['uglify']
    grunt.registerTask "styleguide", ['less','styleguidejs']

    # Config
    grunt.initConfig

        pkg: grunt.file.readJSON "package.json"

        clean:
            options:
                force: true
            spritePre:
                src: [
                    "../web/assets/sprite"
                    "sprite/optim"
                ]
            spritePost: 
                src: [
                    "sprite/optim"
                ]

        styleguidejs:
            styleguide:
                #options:
                #    templateJs: '../public/assets_v2/js/bundle.min.js'
                files:
                    '../web/styleguide/index.html': ['../web/assets/css/global.css']
        
        grunticon:
            options:
                template: 'sprite/grunticon/template.hbs'
                cssprefix: '.sprite-'
                datasvgcss: '../../../frontend/less/sprite/data.svg.less';
                datapngcss: '../../../frontend/less/sprite/data.png.less';
            sprite:
                files: [
                    expand: true,
                    cwd: 'sprite/optim'
                    src: ['*.svg','*.png']
                    dest: "../web/assets/sprite"
                ]

        

        less:
            cssmin:
                options:
                    compress: true
                    cleancss: false
                    sourceMap: false
                    plugins: [require('less-plugin-glob')]
                files:
                    "../web/assets/css/global.min.css": [
                        "less/global.less"
                    ]
            cssfull:
                options:
                    compress: false
                    cleancss: false
                    sourceMap: false
                    plugins: [require('less-plugin-glob')]
                files:
                    "../web/assets/css/global.css": [
                        "less/global.less"
                    ]

        uglify:
            options:
                sourceMap: true
            head:
                files:
                    '../web/assets/js/header.min.js': [
                        'js/header-modules/*.js'
                        'node_modules/picturefill/dist/picturefill.js'
                    ]
            global:
                files:
                    '../web/assets/js/global.min.js': [
                        'node_modules/jquery/dist/jquery.js'
                        'node_modules/flexslider/jquery.flexslider.js'
                        'js/global.js'
                        'js/modules/*.js'
                    ]
        imagemin:
            img:
                files: [
                    expand: true
                    cwd: '../web/assets/img/'
                    src: ['**/*.{png,jpg,gif,svg}']
                    dest: '../web/assets/img/'
                ]
            sprite:
                files: [
                    expand: true
                    cwd: 'sprite/src/'
                    src: ['*.{png,svg}']
                    dest: 'sprite/optim/'
                ]


        svg2png:
            # automagically convert svg's to png for fallback usage
            img:
                files: [ 
                    src: ['../web/assets/img/**/*.svg']
                ]

        watch:
            css:
                files: ['less/**/*.less']
                tasks: ['less','styleguidejs']
            js:
                files: ['js/**/*.js']
                tasks: ['uglify']
            svg2png:
                files: ["../web/assets/img/**/*.svg"]
                tasks: ["svg2png:img"]
            grunticon:
                files: ['sprite/src/*.svg','sprite/src/*.png']
                tasks: ['sprite']
            gruntfile:
                files: ['Gruntfile.coffee']
                tasks: ['uglify','less','styleguidejs']

