# Default site #

### Wat moet je allemaal aanpassen na het uitchecken? ###

* De configs in "app/config/*"
* De .env file. Die word nooit mee gecommit. Zet hierin "dev" of "vagrant"

### Koppel de files aan de GIT repository van de klant ###

Verwijder de .git map en koppel en aan je klant specifieke repository:


```
#!bash

rm -rf .git

```
```
#!bash

git init

```
```
#!bash

git remote add origin URLNAARJOUWKLANTGITREPOSITORY

```
```
#!bash

git add *

```

Nu kan je alles committen en pushen


---

# Basis elementen

## Twig macro's

### Downloads list
Toon een set downloads in een ul/li list. Optioneel met een caption

Usage: `{{blocks.downloads(content.downloads,'Downloads')}}`

### Picture - Responsive images
Automagically generated responsive image with the picture element. Makes only sense for images larger than 800px in width.

Usage: `{{ blocks.picture(content.mainImage|first,'banner_home') }}`

## Twig parts

### default-thumb
Usage:
```
{% include 'parts/default-thumb.html.twig' with {"item":item} only %}
```

Create a custom part `parts/extension-thumb.html.twig` for extra functionality:
```
{% extends '::parts/default-thumb.html.twig' %}
{% block thumbimage %}
  <img src="{{item.icon|first}}" alt=" ">
{% endblock %}
```




### Contact? (misschien niet generiek genoeg)