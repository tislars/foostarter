<?php

use Reprovinci\Nexys\CMSAppKernel;
use SiteBundle\SiteBundle;


/**
 * Class LocalAppKernel
 */
class LocalAppKernel extends CMSAppKernel
{
    /**
     * {@inheritdoc}
     */
    public function registerBundles()
    {
        $bundles = parent::registerBundles();

        $bundles[] = new SiteBundle();

        return $bundles;
    }
}