<?php

namespace SiteBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('sitebundle');

        $rootNode->children()
            ->scalarNode('contact_form_recipient')
                ->isRequired()
                ->cannotBeEmpty()
            ->end()
            ->scalarNode('contact_form_subject')
                ->isRequired()
                ->cannotBeEmpty()
            ->end()
            ->scalarNode('contact_form_from')
                ->isRequired()
                ->cannotBeEmpty()
            ->end()
            ->scalarNode('contact_form_message')
                ->isRequired()
                ->cannotBeEmpty()
            ->end()
        ->end();


        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }


}
