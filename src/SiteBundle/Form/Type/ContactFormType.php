<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 07/10/15
 * Time: 16:00
 */

namespace SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactFormType extends abstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', ['attr'=>['required'=>false],'label'=>'Naam'])
            ->add('email', 'email',['label'=>'E-mail adres'])
            ->add('message', 'textarea', ['label'=>'Je bericht'])
            ->add('submit', 'submit', ['label'=>'Verstuur'])
        ;
    }

    public function getName()
    {
        return 'message';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'SiteBundle\Entity\ContactForm',
        ]);
    }

}
