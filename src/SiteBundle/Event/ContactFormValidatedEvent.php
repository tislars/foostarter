<?php

namespace SiteBundle\Event;

use SiteBundle\Entity\ContactForm;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ContactFormValidatedEvent
 * @package SiteBundle\Event
 */
class ContactFormValidatedEvent extends Event
{
    /** @var bool  */
    private $hasErrors = false;

    /** @var contactform  */
    private $contactform;

    /**
     * ContactFormValidatedEvent constructor.
     * @param ContactForm $contactform
     */
    public function __construct(ContactForm $contactform)
    {
        $this->contactform = $contactform;
    }

    /**
     * @return ContactForm
     */
    public function getContactForm()
    {
        return $this->contactform;
    }

    /**
     * @param boolean $hasErrors
     */
    public function setHasErrors($hasErrors)
    {
        $this->hasErrors = (bool)$hasErrors;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return $this->hasErrors;
    }
}
