<?php

namespace SiteBundle\Event;


use Twig_Environment;

/**
 * Class SendContactFormMailOnFormValidatedListener
 * @package EMG\AbcVanHetGeloofBundle\Event
 */
class SendContactFormMailOnFormValidatedListener implements ContactFormMailerInterface
{
    /**
     * @var Twig_Environment
     */
    protected $twig;
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * SendContactFormMailOnFormValidatedListener constructor.
     * @param Twig_Environment $twig
     * @param \Swift_Mailer $mailer
     */
    public function __construct(Twig_Environment $twig, \Swift_Mailer $mailer, $logger)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    /**
     * @param ContactFormValidatedEvent $event
     * @return bool
     */
    public function onFormValidated(ContactFormValidatedEvent $event)
    {

        $contactform = $event->getContactForm();


        $message = \Swift_Message::newInstance()
            ->setSubject($contactform->getSubject())
            ->setFrom($contactform->getFrom())
            ->setTo($contactform->getRecipient())
            ->setBody(
                $this->twig->render(
                    'SiteBundle:ContactForm:mail.html.twig',
                    array(
                        'contactform' => $contactform,
                    )
                ),
                'text/html'

            )
            ->addPart(
                $this->twig->render(
                    'SiteBundle:ContactForm:mail.txt.twig',
                    array(
                        'contactform' => $contactform,
                    )
                )
            );


        $result = $this->mailer->send($message);

        if ($result === 0) {
            $event->setHasErrors(true);
        }

        return (bool)$result;
    }
}
