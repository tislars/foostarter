<?php

namespace SiteBundle\Event;

use SiteBundle\Event\ContactFormValidatedEvent;

/**
 * Interface ContactFormMailerInterface
 * @package SiteBundle\Event
 */
interface ContactFormMailerInterface
{
    /**
     * @param \SiteBundle\Event\ContactFormValidatedEvent $event
     * @return bool
     */
    public function onFormValidated(ContactFormValidatedEvent $event);
}
