<?php
/**
 * Created by PhpStorm.
 * User: Joep
 * Date: 03/15/16
 * Time: 09:58
 */

namespace SiteBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ContactForm
 * @package SiteBundle\Entity
 */
class ContactForm
{
    const IS_VALIDATED = 'sitebundle.events.contactform.isvalid';

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $message;

    /**
     * @var string
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    private $recipient;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $from;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $subject;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param mixed $recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }
}
