<?php

namespace SiteBundle\ContentDefinition;

use Reprovinci\Nexys\CMSBundle\ContentDefinition\ContentDefinition;

/**
 * Class GlobalContentDefinition
 * @package EMG\AbcVanHetGeloofBundle\ContentDefinition
 */
class GlobalContentDefinition extends ContentDefinition
{
	public function getImage()
    {
        if (!empty($this->mainImage) && count($this->mainImage) > 0) {
            return $this->mainImage->first();
        }
        return null;
    }

    /**
     * @return boolean
     */
    public function hasSidebar()
    {
        $hasSidebar = false;
        if (isset($this->downloads) && count($this->downloads) > 0) {
            $hasSidebar = true;
        } elseif (isset($this->sidebarBlocks) && count($this->sidebarBlocks) > 0) {
            $hasSidebar = true;
        }
        return $hasSidebar;
    }

    /**
     * @return string
     */
    public function mainColClass()
    {
        if (isset($this->downloads) && count($this->downloads) > 0) {
            return "col-sm-7";
        } else {
            return "col-sm-9";
        }
    }

    /**
     *
     * node depth
     *
     */
    
    public function nodeDepth()
    {
        if ( empty($this->node) ) { return false; }
        return substr_count($this->node->getPath(), '/');
    }

    /**
     *
     * @return string
     *
     */
    public function parentUrl()
    {
        preg_match('/^\/(.*(?=\/))*/', $this->node->getPath(), $matches);
        return $matches[0];
    } 
    
}

/**
 *
 * set audience cookie when GET var set_audience is set
 * TODO refactor this to the right place
 */

// if (!empty($_GET['set_audience'])) {
//     setcookie('abc_audience', htmlentities($_GET['set_audience']), 0, '/');
// }
// if (empty($_COOKIE['abc_translation']) && !empty($_COOKIE['abc_audience'])) {
//     setcookie('abc_translation', 'sv', 0, '/');
// }
