<?php

namespace SiteBundle\ContentDefinition;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation as JMS;
use Reprovinci\Nexys\CMSBundle\ContentDefinition\ContentDefinition;
use Reprovinci\Nexys\CMSBundle\ContentDefinition\DataSource\FilteredContentDataSource;
use Reprovinci\Nexys\CMSBundle\ContentDefinition\DataSource\RelatedContentDataSource;
use Reprovinci\Nexys\CMSBundle\ContentDefinition\DataSource\RelatedFileDataSource;
use Reprovinci\Nexys\CMSBundle\Entity\Document;
use Symfony\Component\Validator\Constraints as Assert;
use Reprovinci\Nexys\CMSBundle\CustomAnnotation\FormAnnotation AS FormRules;
use Reprovinci\Nexys\CMSBundle\CustomAnnotation\RelatedAnnotation AS RelatedContent;
use Reprovinci\Nexys\CMSBundle\CustomAnnotation\DataSourceAnnotation AS DataSource;

/**
 * Class Home
 * @package SiteBundle\ContentDefinition
 */
class SidebarBlock extends ContentDefinition
{
    /**
     * @var string
     * @Exclude
     */
    protected $category = 'home';

    /**
     * @var RelatedFileDataSource
     *
     * @Exclude
     * @FormRules(inputType = "dropzoneFile", options = {"required" : false, "label" : "Afbeelding (niet verplicht)", "limit":"1","acceptedFiles": ".jpg,.jpeg,.png"})
     * @DataSource(type="RelatedFileDataSource")
     */
    protected $mainImage;

    /**
     * @var string
     *
     * @JMS\Type("string")
     * @FormRules(inputType = "text", options = {"label":"Titel", "required":false})
     * @Assert\Length(
     *      min = 1,
     *      max = 50,
     *      minMessage = "This field must contain at least {{ limit }} characters.",
     *      maxMessage = "This field cannot consist of more then {{ limit }} characters.")
     */
    protected $title;

    /**
     * @var string
     *
     * @JMS\Type("string")
     * @FormRules(inputType = "textarea", options = {"label":"Tekst", "required":false})
     */
    protected $body;

    /**
     * @var string
     *
     * @JMS\Type("string")
     * @FormRules(inputType = "text", options = {"label":"URL waar je naar toe gaat als je op het blok klikt (niet verplicht)", "required":false})
     */
    protected $link;

    /**
     * @var string
     *
     * @JMS\Type("string")
     * @FormRules(inputType = "text", options = {"label":"Tekst om een knop van te maken (niet verplicht, werkt alleen als er een URL is ingevoerd)", "required":false})
     */
    protected $buttonText;



    public function getName()
    {
        return $this->title;
    }

    public function getCategory(){
        return $this->category;
    }

}