<?php

namespace SiteBundle\ContentDefinition;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation as JMS;
use Reprovinci\Nexys\CMSBundle\ContentDefinition\ContentDefinition;
use Reprovinci\Nexys\CMSBundle\ContentDefinition\DataSource\FilteredContentDataSource;
use Reprovinci\Nexys\CMSBundle\ContentDefinition\DataSource\RelatedContentDataSource;
use Reprovinci\Nexys\CMSBundle\ContentDefinition\DataSource\RelatedFileDataSource;
use Reprovinci\Nexys\CMSBundle\Entity\Document;
use Symfony\Component\Validator\Constraints as Assert;
use Reprovinci\Nexys\CMSBundle\CustomAnnotation\FormAnnotation AS FormRules;
use Reprovinci\Nexys\CMSBundle\CustomAnnotation\RelatedAnnotation AS RelatedContent;
use Reprovinci\Nexys\CMSBundle\CustomAnnotation\DataSourceAnnotation AS DataSource;

/**
 * Class Home
 * @package SiteBundle\ContentDefinition
 */
class Infopage extends GlobalContentDefinition
{
    /**
     * @var string
     * @Exclude
     */
    protected $category = 'home';

    /**
     * @Exclude
     * @FormRules(inputType = "node")
     * @DataSource(type="RelatedNodeDataSource")
     */
    protected $node;

    /**
     * @var string
     *
     * @JMS\Type("string")
     * @FormRules(inputType = "text", options = {"label":"Titel", "required":true})
     * @Assert\Length(
     *      min = 1,
     *      max = 100,
     *      minMessage = "This field must contain at least {{ limit }} characters.",
     *      maxMessage = "This field cannot consist of more then {{ limit }} characters.")
     */
    protected $title;

    /**
     * @var RelatedFileDataSource
     *
     * @Exclude
     * @FormRules(inputType = "dropzoneFile", options = {"required" : true, "label" : "+ Voeg een afbeelding toe", "limit":"1","acceptedFiles": ".jpg,.jpeg,.png"})
     * @DataSource(type="RelatedFileDataSource")
     */
    protected $mainImage;

    /**
     * @var string
     *
     * @JMS\Type("string")
     * @FormRules(inputType = "text", options = {"required" : false, "label" : "Subtitel"})
     */
    protected $subtitle;


    /**
     * @var string
     *
     * @JMS\Type("string")
     * @FormRules(inputType = "textarea", options = {"label":"Introtekst", "required":false, "editor":false})
     */
    protected $intro;

    /**
     * @var string
     *
     * @JMS\Type("string")
     * @FormRules(inputType = "textarea", options = {"label":"Tekst", "required":false})
     */
    protected $body;

    /**
     * @var RelatedFileDataSource
     *
     * @Exclude
     * @FormRules(inputType = "dropzoneFile", options = {"required" : true, "label" : "Downloads (PDF)", "limit":"5","acceptedFiles": ".pdf"})
     * @DataSource(type="RelatedFileDataSource")
     */
    protected $downloads;

    /**
     * @var RelatedContentDataSource
     *
     * @Exclude
     * @FormRules(inputType = "relatedContent", options = {"types" : {"sidebarblock"}, "heading" : "Sidebar Elementen"})
     * @DataSource(type="RelatedContentDataSource")
     */
    protected $sidebarBlocks;

    /**
     * @var string
     *
     * @Exclude
     */
    protected $template = 'SiteBundle:Default:infopage.html.twig';


    /**
     * @return mixed
     */
    public function getNode(){
        return $this->node;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->intro;
    }

    public function getName()
    {
        return $this->title;
    }

    public function getCategory(){
        return $this->category;
    }

}