<?php

/**
 * Created by PhpStorm.
 * User: Joep
 * Date: 07/10/15
 * Time: 15:50
 */

namespace SiteBundle\Controller;

use SiteBundle\Entity\ContactForm;
use SiteBundle\Event\ContactFormValidatedEvent;
use SiteBundle\Form\Type\ContactFormType;
use Reprovinci\Nexys\CMSBundle\Controller\_Default\DefaultController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/contact/contactform")
 */
class ContactFormController extends DefaultController
{

    /**
     * Finds pages with search string in any field
     *
     * @Template()
     * @Route(
     *      name = "send_contactform")
     */
    public function submitAction(Request $request)
    {
        $contactform = new ContactForm();
        $contactform->setRecipient($this->container->getParameter('sitebundle.contact_form_recipient'));
        $contactform->setFrom($this->container->getParameter('sitebundle.contact_form_from'));
        $contactform->setSubject($this->container->getParameter('sitebundle.contact_form_subject'));

        $form = $this->createForm(new ContactFormType(), $contactform);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $contactform = $form->getData();

            $event = new ContactFormValidatedEvent($contactform);
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch(ContactForm::IS_VALIDATED, $event);

            if ($event->hasErrors() === false) {
                $this->addFlash('success', $this->trans('Uw bericht is ontvangen en zal worden verwerkt.'));
            } else {
                $this->addFlash('error', $this->trans('Er ging iets mis met het verwerken van uw bericht.'));
            }
        } else {
            $this->addFlash('error', $this->trans('U heeft niet alle velden correct ingevuld'));
        }

        return $this->redirect($request->headers->get('referer').'#contactform');
    }

    /**
     * Renders contactform
     *
     * @Template()
     *
     * @return array
     */
    public function formAction(Request $request)
    {
        $question = new ContactForm();
        $form = $this->createForm(new ContactFormType(), $question);
        $form->handleRequest($request);

        return array(
            'form' => $form->createView(),
        );
    }
}
