<?php

namespace SiteBundle;

use SiteBundle\DependencyInjection\SiteBundleExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SiteBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new SiteBundleExtension();
    }
}
